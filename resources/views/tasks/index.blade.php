@extends('layouts.master')
@section('content')
    <div class="row mt-5">
        <div class="col-md-6">
            @if(session()->has('msg'))
                <div class="alert alert-success">
                    {{session()->get('msg')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h3>Add Task</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('taskCreate')}}" method="POST">
                        @csrf
                        <div class="form-group">
                             <label for="task">Task</label>
                            <input type="text" name="title" id="task" placeholder="Task"
                                   class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" >
                            <div class="invalid-feedback">
                                The title field is required
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>View Tasks</h3>
                </div>
                <div class="card-body">
                   <table class="table table-bordered">
                       <tr>
                           <th>Task</th>
                           <th style="width:2em;">Action</th>
                       </tr>
                       @foreach($tasks as $task)
                       <tr>
                           <td>{{$task->title}}</td>
                           <form action="{{route('taskDelete', $task->id)}}" method="POST" >
                               @csrf
                               <td><button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button></td>
                           </form>
                       </tr>
                       @endforeach
                   </table>
                </div>
            </div>
            @if(session()->has('mess'))
                <div class="alert alert-success mt-3">
                    {{session()->get('mess')}}
                </div>
            @endif
        </div>
    </div>

@endsection
