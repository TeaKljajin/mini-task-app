<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function index(){
        $tasks = Task::all();
        return view('tasks.index',compact('tasks'));
    }

    public function store(Request $request){
        $request->validate([
            'title'=>'required'
        ]);

        $input = $request->all();
        Task::create($input);
        session()->flash('msg','Task has been created');
        return redirect('/');
    }

    public function destroy($id){
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('/')->with('mess', 'Task has been deleted');
    }
}
